<?php

namespace Agion\SpecialPrices\Plugin;

class QuoteItemPrice
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    private $customerSession;

    /**
     * @var \Agion\SpecialPrices\Model\Calculator\UomSpecialPriceCalculator
     */
    private $uomSpecialPriceCalculator;

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    private $productRepository;
    /**
     * @var \Magento\Checkout\Model\Session
     */
    private $checkoutSession;
    /**
     * @var \Magento\Quote\Model\ResourceModel\QuoteFactory
     */
    private $quoteResourceFactory;
    /**
     * @var \Magento\Quote\Api\Data\CartInterfaceFactory
     */
    private $quoteFactory;
    /**
     * @var \Magento\Framework\App\Helper\AbstractHelper
     */
    private $priceListPriceHelper;

    public function __construct(
        \Magento\Customer\Model\Session $customerSession,
        \Agion\SpecialPrices\Model\Calculator\UomSpecialPriceCalculator $uomSpecialPriceCalculator,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Quote\Api\Data\CartInterfaceFactory $quoteFactory,
        \Magento\Quote\Model\ResourceModel\QuoteFactory $quoteResourceFactory,
        \Magento\Framework\App\Helper\AbstractHelper $priceListPriceHelper
    ) {
        $this->customerSession = $customerSession;
        $this->uomSpecialPriceCalculator = $uomSpecialPriceCalculator;
        $this->productRepository = $productRepository;
        $this->checkoutSession = $checkoutSession;
        $this->quoteResourceFactory = $quoteResourceFactory;
        $this->quoteFactory = $quoteFactory;
        $this->priceListPriceHelper = $priceListPriceHelper;
    }

    public function afterSetQty($subject, $result, $qty)
    {
        $customerId = $this->customerSession->isLoggedIn() ? $this->customerSession->getCustomerId() : null;

        /** @var \Magento\Quote\Model\Quote\Item $subject */
        if ($subject->getOptionByCode('additional_options') && strpos($subject->getOptionByCode('additional_options')->getValue(), 'selected_uom') !== false) {
            $quoteItemOptions = json_decode($subject->getOptionByCode('additional_options')->getValue(), true);

            if (!empty($quoteItemOptions['selected_uom']['uom_data']['external_id'])) {
                $basePrice = null;
                /**
                 * Price List Base Price Support
                 */
                if (get_class($this->priceListPriceHelper) == 'Epartment\MultipleProductPrices\Helper\Prices') {
                    $priceListBasePrice = $this->priceListPriceHelper->getProductPrices($subject->getProductId(), $quoteItemOptions['selected_uom']['uom_data']['unit_id']);
                    if ($priceListBasePrice !== null) {
                        $basePrice = doubleval($priceListBasePrice->getDataModel()->getProductPrice());
                    }
                }

                $productUomPrice = $this->uomSpecialPriceCalculator->calculate(
                    $subject->getProductId(),
                    $customerId,
                    $basePrice,
                    $qty,
                    $quoteItemOptions['selected_uom']['uom_data']['external_id']
                );

                if ($productUomPrice) {
                    $subject->setCustomPrice(doubleval($productUomPrice));
                    $subject->setOriginalCustomPrice(doubleval($productUomPrice));
                    $subject->getProduct()->setIsSuperMode(true);
                }
            }
        }
    }
}
