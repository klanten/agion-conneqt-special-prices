<?php

namespace Agion\SpecialPrices\Plugin;

class LoadExtensionAttributeData
{
    /** @var \Conneqt\SpecialPrices\Api\Data\SpecialPriceExtensionFactory */
    protected $specialPriceExtensionFactory;

    public function __construct(
        \Conneqt\SpecialPrices\Api\Data\SpecialPriceExtensionFactory $specialPriceExtensionFactory
    ) {
        $this->specialPriceExtensionFactory = $specialPriceExtensionFactory;
    }

    public function afterGetExtensionAttributes(
        \Conneqt\SpecialPrices\Api\Data\SpecialPriceInterface $specialPrice,
        \Conneqt\SpecialPrices\Api\Data\SpecialPriceExtensionInterface $specialPriceExtension = null
    ) {
        if ($specialPriceExtension === null) {
            $specialPriceExtension = $this->specialPriceExtensionFactory->create();
        }

        return $specialPriceExtension;
    }
}