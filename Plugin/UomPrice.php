<?php

namespace Agion\SpecialPrices\Plugin;

class UomPrice
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    private $customerSession;

    /**
     * @var \Agion\SpecialPrices\Model\Calculator\UomSpecialPriceCalculator
     */
    private $uomSpecialPriceCalculator;

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    private $productRepository;
    /**
     * @var \Magento\Checkout\Model\Session
     */
    private $checkoutSession;
    /**
     * @var \Magento\Quote\Model\ResourceModel\QuoteFactory
     */
    private $quoteResourceFactory;
    /**
     * @var \Magento\Quote\Api\Data\CartInterfaceFactory
     */
    private $quoteFactory;

    public function __construct(
        \Magento\Customer\Model\Session $customerSession,
        \Agion\SpecialPrices\Model\Calculator\UomSpecialPriceCalculator $uomSpecialPriceCalculator,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Quote\Api\Data\CartInterfaceFactory $quoteFactory,
        \Magento\Quote\Model\ResourceModel\QuoteFactory $quoteResourceFactory
    ) {
        $this->customerSession = $customerSession;
        $this->uomSpecialPriceCalculator = $uomSpecialPriceCalculator;
        $this->productRepository = $productRepository;
        $this->checkoutSession = $checkoutSession;
        $this->quoteResourceFactory = $quoteResourceFactory;
        $this->quoteFactory = $quoteFactory;
    }

    public function afterGetPrice($subject, $result)
    {
        $qty = 1;
        /** @var \Magento\Quote\Model\ResourceModel\Quote $quoteResource */
        $quoteResource = $this->quoteResourceFactory->create();
        $quote = $this->quoteFactory->create();
        $quoteResource->loadByCustomerId($quote, $this->customerSession->getCustomerId());

        /** @var \Magento\Quote\Model\Quote\Item $item */
        foreach ($quote->getItemsCollection() as $item) {
            if ($item->getOptionByCode('additional_options') && strpos($item->getOptionByCode('additional_options')->getValue(), 'selected_uom') !== false) {
                $quoteItemOptions = json_decode($item->getOptionByCode('additional_options')->getValue(), true);

                if ($quoteItemOptions['selected_uom']['uom_id'] == $subject->getUnitId()) {
                    $qty = $item->getQty() + 1;
                }
            }
        }

        /** @var \Magento\Catalog\Model\Product $product */
        $product = $this->productRepository->getById($subject->getProductId());
        $customerId = $this->customerSession->isLoggedIn() ? $this->customerSession->getCustomerId() : null;

        $productUomPrice = $this->uomSpecialPriceCalculator->calculate(
            $subject->getProductId(),
            $customerId,
            $result,
            $qty,
            $subject->getExternalId()
        );

        if ($productUomPrice) {
            return doubleval($productUomPrice);
        }

        return $result;
    }
}
