<?php

namespace Agion\SpecialPrices\Controller\Adminhtml\Index;

class Delete extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = 'Agion_SpecialPrices::agion_specialprices_menu';

    protected $resultPageFactory;

    /**
     * @var \Conneqt\SpecialPrices\Model\ResourceModel\SpecialPrice\CollectionFactory
     */
    private $baseSpecialPriceCollectionFactory;

    /**
     * @var \Conneqt\SpecialPrices\Model\ResourceModel\SpecialPriceFactory
     */
    private $baseSpecialPriceResourceFactory;

    /**
     * @var \Agion\SpecialPrices\Model\ResourceModel\SpecialPriceData\CollectionFactory
     */
    private $specialPriceDataCollectionFactory;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Conneqt\SpecialPrices\Model\ResourceModel\SpecialPrice\CollectionFactory $baseSpecialPriceCollectionFactory,
        \Conneqt\SpecialPrices\Model\ResourceModel\SpecialPriceFactory $baseSpecialPriceResourceFactory,
        \Agion\SpecialPrices\Model\ResourceModel\SpecialPriceData\CollectionFactory $specialPriceDataCollectionFactory
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);

        $this->baseSpecialPriceCollectionFactory = $baseSpecialPriceCollectionFactory;
        $this->baseSpecialPriceResourceFactory = $baseSpecialPriceResourceFactory;
        $this->specialPriceDataCollectionFactory = $specialPriceDataCollectionFactory;
    }

    public function execute()
    {
        $specialPrices = $this->specialPriceDataCollectionFactory->create()
                                ->addFieldToFilter('id', ['in' => $this->getRequest()->getParam('selected')]);
        $baseIds = \array_map(function ($specialPrice) {
            return $specialPrice['specialprice_id'];
        }, $specialPrices->toArray(['specialprice_id'])['items']);

        $baseSpecialPrices = $this->baseSpecialPriceCollectionFactory->create()
                                    ->addFieldToFilter('entity_id', ['in' => $baseIds]);

        $baseSpecialPriceResource = $this->baseSpecialPriceResourceFactory->create();
        foreach ($baseSpecialPrices as $baseSpecialPrice) {
            $baseSpecialPriceResource->delete($baseSpecialPrice);
        }

        $result = $this->resultRedirectFactory->create();
        $result->setPath('agion_specialprices/index/index');

        return $result;
    }
}
