<?php

namespace Agion\SpecialPrices\Controller\Adminhtml\Index;
class Index extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = 'Agion_SpecialPrices::agion_specialprices_menu';

    protected $resultPageFactory;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->prepend(__("AGION Special Prices"));
        return $resultPage;
    }
}
