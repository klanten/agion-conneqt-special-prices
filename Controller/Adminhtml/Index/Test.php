<?php

namespace Agion\SpecialPrices\Controller\Adminhtml\Index;
class Test extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = 'Agion_SpecialPrices::agion_specialprices_menu';

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    private $jsonFactory;

    /**
     * @var \Magento\Framework\View\LayoutFactory
     */
    private $layoutFactory;

    /**
     * @var \Agion\SpecialPrices\Model\Calculator\UomSpecialPriceCalculator
     */
    private $calculator;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    private $productCollectionFactory;

    /**
     * @var \Epartment\MultipleProductPrices\Helper\Prices
     */
    private $priceListPriceHelper;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $jsonFactory,
        \Magento\Framework\View\LayoutFactory $layoutFactory,
        \Agion\SpecialPrices\Model\Calculator\UomSpecialPriceCalculator $calculator,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Epartment\MultipleProductPrices\Helper\Prices $priceListPriceHelper
    ) {
        parent::__construct($context);

        $this->jsonFactory = $jsonFactory;
        $this->layoutFactory = $layoutFactory;
        $this->calculator = $calculator;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->priceListPriceHelper = $priceListPriceHelper;
    }

    public function execute()
    {
        $content = $this->layoutFactory->create()
            ->createBlock(
                \Agion\SpecialPrices\Block\Adminhtml\TestResult::class
            );

        $response = $this->jsonFactory->create();

        try {
            $customerId = $this->getRequest()->getParam('customer');
            $productId = $this->productCollectionFactory->create()
                ->addFieldToFilter('sku', ['eq' => $this->getRequest()->getParam('sku')])
                ->getFirstItem()->getId();
            $qty = !empty($this->getRequest()->getParam('quantity')) ? $this->getRequest()->getParam('quantity') : 1;
            $uomCode = !empty($this->getRequest()->getParam('uom_code')) ? $this->getRequest()->getParam('uom_code') : null;

            $basePrice = 0;
            $priceListBasePrice = $this->priceListPriceHelper->getProductPricesForCustomer($productId, $customerId, null, $uomCode);
            if ($priceListBasePrice !== null) {
                $basePrice = doubleval($priceListBasePrice->getDataModel()->getProductPrice());
            }

            $calculatedPrice = $this->calculator->calculate($productId, $customerId, $basePrice, $qty, $uomCode);
            $content->setData('calculated_price', $calculatedPrice);

            $content->setData('arguments', $this->calculator->getUsedArguments());

            return $response->setData(['calculated' => $calculatedPrice, 'result' => $content->toHtml(), 'status' => 'success']);
        } catch (\Exception $ex) {
            return $response->setData(['result' => $ex->getMessage(), 'status' => 'error']);
        }
    }
}
