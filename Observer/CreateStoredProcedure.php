<?php

namespace Agion\SpecialPrices\Observer;

class CreateStoredProcedure implements \Magento\Framework\Event\ObserverInterface
{
    const CONFIG_PATH_STORED_PROCEDURE = 'conneqt_specialprices/settings/stored_procedure';

    /** @var \Magento\Framework\App\Config\ScopeConfigInterface */
    protected $scopeConfig;

    /** @var \Agion\SpecialPrices\Helper\StoredProcedure */
    protected $storedProcedureHelper;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Agion\SpecialPrices\Helper\StoredProcedure $storedProcedureHelper
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->storedProcedureHelper = $storedProcedureHelper;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $websiteId = empty($observer->getWebsite()) ? 0 : $observer->getWebsite();
        $changedPaths = $observer->getChangedPaths();

        if (in_array(self::CONFIG_PATH_STORED_PROCEDURE, $changedPaths)) {
            $storedProcedure = $this->scopeConfig->getValue(self::CONFIG_PATH_STORED_PROCEDURE, \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE, $websiteId);
            $this->storedProcedureHelper->createStoredProcedure($this->storedProcedureHelper->getStoredProcedureName($websiteId), $storedProcedure);
        }
    }
}
