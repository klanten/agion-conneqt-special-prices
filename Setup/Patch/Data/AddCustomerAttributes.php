<?php

namespace Agion\SpecialPrices\Setup\Patch\Data;

class AddCustomerAttributes implements \Magento\Framework\Setup\Patch\DataPatchInterface
{
    /** @var \Magento\Framework\Setup\ModuleDataSetupInterface */
    protected $moduleDataSetup;

    /** @var \Magento\Customer\Setup\CustomerSetupFactory */
    protected $customerSetupFactory;

    public function __construct(
        \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup,
        \Magento\Customer\Setup\CustomerSetupFactory $customerSetupFactory
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->customerSetupFactory = $customerSetupFactory;
    }

    public static function getDependencies()
    {
        return [];
    }

    public function getAliases()
    {
        return [];
    }

    public function apply()
    {
        $customerSetup = $this->customerSetupFactory->create(['setup' => $this->moduleDataSetup]);

        $customerSetup->addAttribute(
            \Magento\Customer\Model\Customer::ENTITY,
            'pricelist_id',
            [
                'type' => 'varchar',
                'label' => 'Pricelist ID',
                'input' => 'text',
                'required' => false,
                'sort_order' => 1000,
                'visible' => true,
                'system' => false,
                'user_defined' => false
            ]
        );

        $customerSetup->addAttribute(
            \Magento\Customer\Model\Customer::ENTITY,
            'customergroup_id',
            [
                'type' => 'varchar',
                'label' => 'Customer Group ID',
                'input' => 'text',
                'required' => false,
                'sort_order' => 1000,
                'visible' => true,
                'system' => false,
                'user_defined' => false
            ]
        );

        $customerSetup->addAttribute(
            \Magento\Customer\Model\Customer::ENTITY,
            'effective_discount',
            [
                'type' => 'varchar',
                'label' => 'Effective Discount',
                'input' => 'text',
                'required' => false,
                'sort_order' => 1000,
                'visible' => true,
                'system' => false,
                'user_defined' => false
            ]
        );

        $customerSetup->addAttribute(
            \Magento\Customer\Model\Customer::ENTITY,
            'exclude_discount_groups',
            [
                'type' => 'int',
                'label' => 'Exclude Discount Groups',
                'input' => 'boolean',
                'source_model' => \Magento\Eav\Model\Entity\Attribute\Source\Boolean::class,
                'required' => false,
                'sort_order' => 1000,
                'visible' => true,
                'system' => false,
                'user_defined' => false
            ]
        );

        $customerSetup->addAttribute(
            \Magento\Customer\Model\Customer::ENTITY,
            'parent_customer_external_id',
            [
                'type' => 'varchar',
                'label' => 'Parent Customer External ID',
                'input' => 'text',
                'required' => false,
                'sort_order' => 1000,
                'visible' => true,
                'system' => false,
                'user_defined' => false
            ]
        );
    }
}
