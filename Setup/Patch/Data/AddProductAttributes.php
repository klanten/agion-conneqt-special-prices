<?php

namespace Agion\SpecialPrices\Setup\Patch\Data;

class AddProductAttributes implements \Magento\Framework\Setup\Patch\DataPatchInterface
{
    /** @var \Magento\Framework\Setup\ModuleDataSetupInterface */
    protected $moduleDataSetup;

    /** @var \Magento\Eav\Setup\EavSetupFactory */
    protected $eavSetupFactory;

    public function __construct(
        \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup,
        \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
    }

    public static function getDependencies()
    {
        return [];
    }

    public function getAliases()
    {
        return [];
    }

    public function apply()
    {
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);

        $eavSetup->addAttribute('catalog_product', 'manufacturer_id', [
            'type' => 'varchar',
            'label' => 'Manufacturer ID',
            'input' => 'text',
            'used_in_product_listing' => true,
            'user_defined' => false,
            'required' => false,
        ]);

        $eavSetup->addAttribute('catalog_product', 'itemgroup_id', [
            'type' => 'varchar',
            'label' => 'Itemgroup ID',
            'input' => 'text',
            'used_in_product_listing' => true,
            'user_defined' => false,
            'required' => false,
        ]);
    }
}