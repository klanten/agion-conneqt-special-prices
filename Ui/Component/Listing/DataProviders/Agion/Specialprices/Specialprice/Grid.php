<?php
namespace Agion\SpecialPrices\Ui\Component\Listing\DataProviders\Agion\Specialprices\Specialprice;


/**
 * Class Grid
 */
class Grid extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        \Agion\SpecialPrices\Model\ResourceModel\SpecialPriceData\CollectionFactory $collectionFactory,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $collectionFactory->create();
    }
}
