<?php

namespace Agion\SpecialPrices\Api\Data;

interface SpecialPriceDataInterface
{
    const FIELD_ID = 'id';
    const FIELD_SPECIALPRICE_ID = 'specialprice_id';
    const FIELD_ERP_ID = 'erp_id';
    const FIELD_WEBSITE_ID = 'website_id';
    const FIELD_CUSTOMER_EXTERNAL_ID = 'customer_external_id';
    const FIELD_SKU = 'sku';
    const FIELD_ITEMGROUP_ID = 'itemgroup_id';
    const FIELD_CUSTOMERGROUP_ID = 'customergroup_id';
    const FIELD_PRICELIST_ID = 'pricelist_id';
    const FIELD_MANUFACTURER_ID = 'manufacturer_id';
    const FIELD_DISCOUNT_TYPE = 'discount_type';
    const FIELD_DATE_FROM = 'date_from';
    const FIELD_DATE_TO = 'date_to';
    const FIELD_QUANTITY = 'quantity';
    const FIELD_VALUE = 'value';

    /**
     * @return int
     */
    public function getId();

    /**
     * @return mixed
     */
    public function getSpecialPriceId();

    /**
     * @var $specialPriceId int
     * @return \Agion\SpecialPrices\Api\Data\SpecialPriceDataInterface
     */
    public function setSpecialPriceId($specialPriceId);

    /**
     * @return string
     */
    public function getErpId();

    /**
     * @param $erpId string
     * @return \Agion\SpecialPrices\Api\Data\SpecialPriceDataInterface
     */
    public function setErpId($erpId);

    /**
     * @return int
     */
    public function getWebsiteId();

    /**
     * @param $websiteId int
     * @return \Agion\SpecialPrices\Api\Data\SpecialPriceDataInterface
     */
    public function setWebsiteId($websiteId);

    /**
     * @return string
     */
    public function getCustomerExternalId();

    /**
     * @param $customerExternalId string
     * @return \Agion\SpecialPrices\Api\Data\SpecialPriceDataInterface
     */
    public function setCustomerExternalId($customerExternalId);

    /**
     * @return string
     */
    public function getSku();

    /**
     * @param $sku string
     * @return \Agion\SpecialPrices\Api\Data\SpecialPriceDataInterface
     */
    public function setSku($sku);

    /**
     * @return string
     */
    public function getItemgroupId();

    /**
     * @param $itemgroupId string
     * @return \Agion\SpecialPrices\Api\Data\SpecialPriceDataInterface
     */
    public function setItemgroupId($itemgroupId);

    /**
     * @return string
     */
    public function getCustomergroupId();

    /**
     * @param $customergroupId string
     * @return \Agion\SpecialPrices\Api\Data\SpecialPriceDataInterface
     */
    public function setCustomergroupId($customergroupId);

    /**
     * @return string
     */
    public function getPricelistId();

    /**
     * @param $pricelistId string
     * @return \Agion\SpecialPrices\Api\Data\SpecialPriceDataInterface
     */
    public function setPricelistId($pricelistId);

    /**
     * @return string
     */
    public function getManufacturerId();

    /**
     * @param $manufacturerId string
     * @return \Agion\SpecialPrices\Api\Data\SpecialPriceDataInterface
     */
    public function setManufacturerId($manufacturerId);

    /**
     * @return string
     */
    public function getDiscountType();

    /**
     * @param $discountType string
     * @return \Agion\SpecialPrices\Api\Data\SpecialPriceDataInterface
     */
    public function setDiscountType($discountType);

    /**
     * @return string
     */
    public function getDateFrom();

    /**
     * @param $dateFrom string
     * @return \Agion\SpecialPrices\Api\Data\SpecialPriceDataInterface
     */
    public function setDateFrom($dateFrom);

    /**
     * @return string
     */
    public function getDateTo();

    /**
     * @param $dateTo string
     * @return \Agion\SpecialPrices\Api\Data\SpecialPriceDataInterface
     */
    public function setDateTo($dateTo);

    /**
     * @return \Agion\SpecialPrices\Api\Data\SpecialPriceDataVolumePriceInterface[]
     */
    public function getPriceVolumes();

    /**
     * @param \Agion\SpecialPrices\Api\Data\SpecialPriceDataVolumePriceInterface[] $priceVolumes
     * @return \Agion\SpecialPrices\Api\Data\SpecialPriceDataInterface
     */
    public function setPriceVolumes($priceVolumes);
}