<?php

namespace Agion\SpecialPrices\Api\Data;

interface SpecialPriceDataVolumePriceInterface
{
    const FIELD_ID = 'id';
    const FIELD_AGION_SPECIALPRICE_ID = 'agion_specialprices_id';
    const FIELD_UOM_CODE = 'uom_code';
    const FIELD_QUANTITY = 'quantity';
    const FIELD_VALUE = 'value';
    const FIELD_EFFECTIVE_DISCOUNT_PERCENTAGE = 'effective_discount_percentage';

    /**
     * @return int
     */
    public function getId();

    /**
     * @return mixed
     */
    public function getAgionSpecialPriceId();

    /**
     * @var $specialPriceId int
     * @return \Agion\SpecialPrices\Api\Data\SpecialPriceDataInterface
     */
    public function setAgionSpecialPriceId($specialPriceId);

    /**
     * @return string|null
     */
    public function getUomCode();

    /**
     * @param $uomCode string
     * @return \Agion\SpecialPrices\Api\Data\SpecialPriceDataInterface
     */
    public function setUomCode($uomCode);

    /**
     * @return int
     */
    public function getQuantity();

    /**
     * @param $quantity int
     * @return \Agion\SpecialPrices\Api\Data\SpecialPriceDataInterface
     */
    public function setQuantity($quantity);

    /**
     * @return double
     */
    public function getValue();

    /**
     * @param $value double
     * @return \Agion\SpecialPrices\Api\Data\SpecialPriceDataInterface
     */
    public function setValue($value);

    /**
     * @return double
     */
    public function getEffectiveDiscountPercentage();

    /**
     * @param $effectiveDiscountPercentage double
     * @return \Agion\SpecialPrices\Api\Data\SpecialPriceDataInterface
     */
    public function setEffectiveDiscountPercentage($effectiveDiscountPercentage);
}
