<?php

namespace Agion\SpecialPrices\Model\ResourceModel\SpecialPriceData;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init(\Agion\SpecialPrices\Model\SpecialPriceData::class, \Agion\SpecialPrices\Model\ResourceModel\SpecialPriceData::class);
    }

    /**
     * @param $specialPriceId
     * @return \Agion\SpecialPrices\Model\SpecialPriceData
     */
    public function findBySpecialPriceId($specialPriceId)
    {
        return $this->addFieldToFilter('specialprice_id', ['in' => [(int) $specialPriceId], $specialPriceId])->getFirstItem();
    }
}
