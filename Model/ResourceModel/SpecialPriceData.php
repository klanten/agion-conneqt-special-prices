<?php

namespace Agion\SpecialPrices\Model\ResourceModel;

class SpecialPriceData extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('agion_specialprices', 'id');
    }
}