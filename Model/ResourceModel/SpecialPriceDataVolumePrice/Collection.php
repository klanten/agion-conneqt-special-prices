<?php

namespace Agion\SpecialPrices\Model\ResourceModel\SpecialPriceDataVolumePrice;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init(\Agion\SpecialPrices\Model\SpecialPriceDataVolumePrice::class, \Agion\SpecialPrices\Model\ResourceModel\SpecialPriceDataVolumePrice::class);
    }

    /**
     * @param $specialPriceId
     * @return \Agion\SpecialPrices\Model\SpecialPriceDataVolumePrice[]
     */
    public function findBySpecialPriceId($specialPriceId)
    {
        return $this->addFieldToFilter('agion_specialprices_id', ['eq' => $specialPriceId])->getItems();
    }
}