<?php

namespace Agion\SpecialPrices\Model;

/**
 * @method \Agion\SpecialPrices\Model\ResourceModel\SpecialPriceDataVolumePrice getResource()
 * @method \Agion\SpecialPrices\Model\ResourceModel\SpecialPriceDataVolumePrice\Collection getCollection()
 */
class SpecialPriceDataVolumePrice extends \Magento\Framework\Model\AbstractModel implements \Agion\SpecialPrices\Api\Data\SpecialPriceDataVolumePriceInterface,
    \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'agion_specialprices_specialpricedatavolumeprice';
    protected $_cacheTag = 'agion_specialprices_specialpricedatavolumeprice';
    protected $_eventPrefix = 'agion_specialprices_specialpricedatavolumeprice';

    protected function _construct()
    {
        $this->_init(\Agion\SpecialPrices\Model\ResourceModel\SpecialPriceDataVolumePrice::class);
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @return mixed
     */
    public function getAgionSpecialPriceId()
    {
        return $this->getData(self::FIELD_AGION_SPECIALPRICE_ID);
    }

    /**
     * @var $specialPriceId int
     * @return \Agion\SpecialPrices\Api\Data\SpecialPriceDataVolumePriceInterface
     */
    public function setAgionSpecialPriceId($specialPriceId)
    {
        $this->setData(self::FIELD_AGION_SPECIALPRICE_ID, $specialPriceId);

        return $this;
    }

    /**
     * @return string
     */
    public function getUomCode()
    {
        return $this->getData(self::FIELD_UOM_CODE);
    }

    /**
     * @param $uomCode string
     * @return \Agion\SpecialPrices\Api\Data\SpecialPriceDataVolumePriceInterface
     */
    public function setUomCode($uomCode)
    {
        $this->setData(self::FIELD_UOM_CODE, $uomCode);

        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->getData(self::FIELD_QUANTITY);
    }

    /**
     * @param $quantity int
     * @return \Agion\SpecialPrices\Api\Data\SpecialPriceDataVolumePriceInterface
     */
    public function setQuantity($quantity)
    {
        $this->setData(self::FIELD_QUANTITY, $quantity);

        return $this;
    }

    /**
     * @return double
     */
    public function getValue()
    {
        return $this->getData(self::FIELD_VALUE);
    }

    /**
     * @param $value double
     * @return \Agion\SpecialPrices\Api\Data\SpecialPriceDataVolumePriceInterface
     */
    public function setValue($value)
    {
        $this->setData(self::FIELD_VALUE, $value);

        return $this;
    }

    /**
     * @return double
     */
    public function getEffectiveDiscountPercentage()
    {
        return $this->getData(self::FIELD_EFFECTIVE_DISCOUNT_PERCENTAGE);
    }

    /**
     * @param $effectiveDiscountPercentage double
     * @return \Agion\SpecialPrices\Api\Data\SpecialPriceDataVolumePriceInterface
     */
    public function setEffectiveDiscountPercentage($effectiveDiscountPercentage)
    {
        $this->setData(self::FIELD_EFFECTIVE_DISCOUNT_PERCENTAGE, $effectiveDiscountPercentage);

        return $this;
    }
}
