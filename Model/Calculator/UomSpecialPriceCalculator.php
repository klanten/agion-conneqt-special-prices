<?php

namespace Agion\SpecialPrices\Model\Calculator;

class UomSpecialPriceCalculator
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /** @var \Magento\Framework\App\Config\ScopeConfigInterface */
    protected $scopeConfig;

    /** @var \Magento\Catalog\Api\ProductRepositoryInterface */
    protected $productRepository;

    /** @var \Magento\Customer\Api\CustomerRepositoryInterface */
    protected $customerRepository;

    /** @var \Agion\SpecialPrices\Helper\StoredProcedure */
    protected $storedProcedureHelper;

    /** @var \Magento\Store\Model\StoreManagerInterface */
    protected $storeManager;

    /**
     * @var \Agion\SpecialPrices\Helper\PlaceholderHelper
     */
    private $priceListPriceHelper;

    private $storedProcedureArguments;

    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Agion\SpecialPrices\Helper\StoredProcedure $storedProcedureHelper,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Helper\AbstractHelper $priceListPriceHelper
    ) {
        $this->logger = $logger;
        $this->scopeConfig = $scopeConfig;
        $this->productRepository = $productRepository;
        $this->customerRepository = $customerRepository;
        $this->storedProcedureHelper = $storedProcedureHelper;
        $this->storeManager = $storeManager;
        $this->priceListPriceHelper = $priceListPriceHelper;
    }

    public function getUsedArguments()
    {
        return $this->storedProcedureArguments;
    }

    /**
     * Recalculates the price for a product
     *
     * @param $productId int
     * @param $customerId int
     * @param $basePrice
     * @param $qty int
     * @param $uomCode string
     * @return double
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function calculate($productId, $customerId, $basePrice, $qty, $uomCode)
    {
        if ($basePrice == false) {
            $basePrice = $this->productRepository->getById($productId)->getData('price');
        }

        if ($customerId === null) {
            return $basePrice;
        }

        /**
         * Price List Base Price Support
         */
        if (get_class($this->priceListPriceHelper) == 'Epartment\MultipleProductPrices\Helper\Prices') {
            $priceListBasePrice = $this->priceListPriceHelper->getProductPrices($productId, null, $uomCode);
            if ($priceListBasePrice !== null) {
                $basePrice = doubleval($priceListBasePrice->getDataModel()->getProductPrice());
            }
        }

        $specialPricesEnabled = $this->scopeConfig->getValue('conneqt_specialprices/settings/active', \Magento\Framework\App\Config\ScopeConfigInterface::SCOPE_TYPE_DEFAULT);

        if ($specialPricesEnabled != 1) {
            return $basePrice;
        }

        if (empty($qty)) {
            $qty = 1;
        }

        if ($customerId !== null) {
            $product = $this->productRepository->getById($productId);
            $customer = $this->customerRepository->getById($customerId);

            $this->storedProcedureArguments = [
                'website_id' => $this->storeManager->getWebsite()->getId(),
                'customer_external_id' => $customer->getCustomAttribute('parent_external_id') ? $customer->getCustomAttribute('parent_external_id')->getValue() : ($customer->getCustomAttribute('external_id') ? $customer->getCustomAttribute('external_id')->getValue() : null),
                'sku' => $product->getSku(),
                'uom_code' => $uomCode,
                'qty' => $qty,
                'date' => date('d-m-y'),
                'store_id' => $this->storeManager->getStore()->getId(),
                'customergroup_id' => $customer->getCustomAttribute('customergroup_id') ? $customer->getCustomAttribute('customergroup_id')->getValue() : null,
                'pricelist_id' => $customer->getCustomAttribute('pricelist_id') ? $customer->getCustomAttribute('pricelist_id')->getValue() : null,
                'effective_discount' => $customer->getCustomAttribute('effective_discount') ? $customer->getCustomAttribute('effective_discount')->getValue() : null,
                'exclude_discount_groups' => $customer->getCustomAttribute('exclude_discount_groups') ? $customer->getCustomAttribute('exclude_discount_groups')->getValue() : null,
                'itemgroup_id' => $product->getCustomAttribute('itemgroup_id') ? $product->getCustomAttribute('itemgroup_id')->getValue() : null,
                'manufacturer_id' => $product->getCustomAttribute('manufacturer_id') ? $product->getCustomAttribute('manufacturer_id')->getValue() : null,
                'base_price' => $basePrice
            ];

            $result = $this->storedProcedureHelper->callStoredProcedure(
                $this->storeManager->getWebsite()->getId(),
                $customer->getCustomAttribute('parent_external_id') ? $customer->getCustomAttribute('parent_external_id')->getValue() : ($customer->getCustomAttribute('external_id') ? $customer->getCustomAttribute('external_id')->getValue() : null),
                $product->getSku(),
                $uomCode,
                $qty,
                date('d-m-y'),
                $this->storeManager->getStore()->getId(),
                $customer->getCustomAttribute('customergroup_id') ? $customer->getCustomAttribute('customergroup_id')->getValue() : null,
                $customer->getCustomAttribute('pricelist_id') ? $customer->getCustomAttribute('pricelist_id')->getValue() : null,
                $customer->getCustomAttribute('effective_discount') ? $customer->getCustomAttribute('effective_discount')->getValue() : null,
                $customer->getCustomAttribute('exclude_discount_groups') ? $customer->getCustomAttribute('exclude_discount_groups')->getValue() : null,
                $product->getCustomAttribute('itemgroup_id') ? $product->getCustomAttribute('itemgroup_id')->getValue() : null,
                $product->getCustomAttribute('manufacturer_id') ? $product->getCustomAttribute('manufacturer_id')->getValue() : null,
                $basePrice
            );

            if (!empty($result)) {
                if (!array_key_exists('PriceType', $result) || !array_key_exists('Value', $result)) {
                    return $basePrice;
                }

                if ($result['PriceType'] == null || $result['Value'] == null) {
                    return $basePrice;
                }

                $value = $result['Value'];
                $type = $result['PriceType'];

                if ($type == 1) {
                    return $value;
                }

                if ($type == 2) {
                    return $basePrice - (($basePrice / 100) * $value);
                }

                if ($type == 3) {
                    return $basePrice - $value;
                }
            }
        }

        return $basePrice;
    }
}
