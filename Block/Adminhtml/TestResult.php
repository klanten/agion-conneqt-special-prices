<?php

namespace Agion\SpecialPrices\Block\Adminhtml;

use Magento\Backend\Block\Template;

class TestResult extends Template
{
    public $_template = 'Agion_SpecialPrices::view/test_result.phtml';

    public function __construct(
        \Magento\Backend\Block\Template\Context $context
    ) {
        parent::__construct($context);
    }
}
