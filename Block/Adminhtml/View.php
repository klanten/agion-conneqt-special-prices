<?php

namespace Agion\SpecialPrices\Block\Adminhtml;

use Magento\Backend\Block\Template;

class View extends Template
{
    public $_template = 'Agion_SpecialPrices::view/tiers.phtml';

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $request;

    /**
     * @var \Agion\SpecialPrices\Model\ResourceModel\SpecialPriceData\CollectionFactory
     */
    private $priceCollectionFactory;

    /**
     * @var \Agion\SpecialPrices\Model\ResourceModel\SpecialPriceDataVolumePrice\CollectionFactory
     */
    private $collectionFactory;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\App\RequestInterface $request,
        \Agion\SpecialPrices\Model\ResourceModel\SpecialPriceData\CollectionFactory $priceCollectionFactory,
        \Agion\SpecialPrices\Model\ResourceModel\SpecialPriceDataVolumePrice\CollectionFactory $collectionFactory
    ) {
        parent::__construct($context);

        $this->request = $request;
        $this->priceCollectionFactory = $priceCollectionFactory;
        $this->collectionFactory = $collectionFactory;
    }

    public function getExternalId()
    {
        return $this->priceCollectionFactory->create()
            ->addFieldToFilter('id', ['eq' => $this->request->getParam('id')])
            ->getFirstItem()->getErpId();
    }

    public function getTiers()
    {
        $specialPriceId = $this->request->getParam('id');

        return $this->collectionFactory->create()
            ->addFieldToFilter('agion_specialprices_id', ['eq' => $specialPriceId])
            ->addOrder('uom_code', 'ASC')
            ->addOrder('quantity', 'ASC');
    }
}
